#! /usr/bin/python3

import sys
import argparse
import time
import datetime

def parseline(l):
    elms = l.split(' ', 1)

    print (datetime.datetime.fromtimestamp(int(float(elms[0]))), elms[1])


def readlog(f):
    line = f.readline()
    while line:
        parseline(line)
        line = f.readline()

def readloop(f):
    while True:
        readlog(f)

def main():

    parser = argparse.ArgumentParser(description="Parse Squid log")
    parser.add_argument('filename')
    a = parser.parse_args()
    print (a.filename)

    with open(a.filename) as f:
        readloop(f)

if __name__ == '__main__':
    main()